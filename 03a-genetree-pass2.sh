#!/bin/bash
#SBATCH -t 24:00:00
#SBATCH -N 8
#SBATCH -c 16
#SBATCH --mem=60g
#SBATCH -C intel

export AGALMA_DB="$PWD/agalma.sqlite"
export BIOLITE_RESOURCES="threads=${SLURM_CPUS_ON_NODE},memory=${SLURM_MEM_PER_NODE}M"
export BIOLITE_HOSTLIST=$(hostlist -e -s, $SLURM_NODELIST)

set -e

mkdir -p scratch
cd scratch

ID=AgalmaExampleTree

agalma treeinform --id $ID
agalma homologize --id $ID
agalma multalign --id $ID
agalma genetree --id $ID
