# Introduction

This repository contains the scripts used to cunduct the analysyes presented in:

> CW Dunn, M Howison, and F Zapata (2013) Agalma: an automated phylogenomics workflow. BMC Bioinformatics 14:330. [doi:10.1186/1471-2105-14-330](http://dx.doi.org/10.1186/1471-2105-14-330).

# Using the scripts

## Dependencies

These scripts require [Agalma](https://bitbucket.org/caseywdunn/agalma) and its 
dependencies. Agalma version 0.3.4 was used for the published analyses.

**On May 9, 2014 this repo was updated with scripts that now run the analysis
with Agalma version 0.4.0. This produces a report that differs from the one
published with the paper, due to numerous improvements made to Agalma between
the 0.3.4 and 0.4.0 releases. In particular, supermatrix occupancy is much
higher in the 0.4.0 analysis.**

**This summary table provides exact git commits for each analysis:**

Analysis   | Agalma version | Scripts commit
--------   | -------------- | --------------
Published  | 0.3.4 ([dc549d2](https://bitbucket.org/caseywdunn/agalma/src/dc549d23161697f9d2a7169cb924076c5908d51a/README.md?at=master)) | [e930b2e](https://bitbucket.org/caseywdunn/dunnhowisonzapata2013/src/e930b2ed665ee1a5e14c6fb9b53609173a011690/README.md?at=master)
2014-05-09 | 0.4.0 ([1fe8064](https://bitbucket.org/caseywdunn/agalma/src/1fe806424f78da4d6fefff7f5167f84cbc2e50aa/README.md?at=master)) | [4a1dfc4](https://bitbucket.org/caseywdunn/dunnhowisonzapata2013/src/4a1dfc44bf323d33f4e5070174b6e934c84950e6/README.md?at=master)

## Executing

The analysis is broken into a series of scripts.
To reproduce the published analysis, execute the scripts in sequence, ie:

    sh 00-catalog.sh
    sh 01-assemble-Abylopsis.sh
    sh 02-assemble-Agalma.sh
    sh 03-assemble-Nanomia.sh
    sh 04-assemble-Physalia.sh
    sh 05-assemble-Prayidae.sh
    sh 06-load-Nematostella.sh
    sh 07-load-Hydra.sh
    sh 08a-phylogeny.sh
    sh 08b-phylogeny.sh
    sh 09-report.sh
    
Scripts 01 to 07 can be run concurrently.
The scripts include, as comments, commands for executing the analyses via the 
[SLURM](https://computing.llnl.gov/linux/slurm/quickstart.html) job scheduler 
installed on the 
[OSCAR cluster](http://www.brown.edu/Departments/CCV/doc/techspecs) at 
Brown University. If you are running the analyses without a job scheduler, then 
these SLURM commands will be ignored. If you are using a job scheduler, you 
will need to edit these commands according to the configuration of your own 
system.

