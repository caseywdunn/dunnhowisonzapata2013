#!/bin/bash
#SBATCH -t 4:00:00
#SBATCH -c 8
#SBATCH --mem=20g
#SBATCH --array=1-2

set -e

export AGALMA_DB="$PWD/agalma.sqlite"
export BIOLITE_RESOURCES="threads=${SLURM_CPUS_ON_NODE},memory=${SLURM_MEM_PER_NODE}M"

IDS=(
	JGI_NEMVEC
	NCBI_HYDMAG
)

ID=${IDS[$SLURM_ARRAY_TASK_ID-1]}
echo $ID

mkdir -p scratch
cd scratch

agalma import --id $ID
agalma translate --id $ID
