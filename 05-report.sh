#!/bin/bash
#SBATCH -t 15

export AGALMA_DB="$PWD/agalma.sqlite"

set -e

mkdir -p reports

agalma tabular_report -a -l -o reports/tabular
agalma resources -i SRX288285 -o reports/SRX288285

agalma report -i AgalmaExampleTree -o reports/AgalmaExampleTree
agalma phylogeny_report -i AgalmaExampleTree -o reports

