#!/bin/sh

set -e

export BIOLITE_RESOURCES="database=$PWD/agalma.sqlite"

mkdir -p data
cd data

EMAIL="mhowison@brown.edu"
bl-sra import -c -e $EMAIL SRX288432 &
bl-sra import -c -e $EMAIL SRX288431 &
bl-sra import -c -e $EMAIL SRX288430 &
bl-sra import -c -e $EMAIL SRX288285 &
bl-sra import -c -e $EMAIL SRX288276 &
wait

wget ftp://ftp.jgi-psf.org/pub/JGI_data/Nematostella_vectensis/v1.0/annotation/transcripts.Nemve1FilteredModels1.fasta.gz
gunzip transcripts.Nemve1FilteredModels1.fasta.gz
mv transcripts.Nemve1FilteredModels1.fasta Nematostella_vectensis_rna.fa
agalma catalog insert --id "JGI_NEMVEC" --paths "Nematostella_vectensis_rna.fa" --species "Nematostella vectensis" --ncbi_id "45351" --itis_id "52498" --library_type "genome" --note "Gene predictions from genome sequencing"

wget http://ftp.ncbi.nih.gov/genomes/Hydra_magnipapillata/RNA/rna.fa.gz
gunzip rna.fa.gz
mv rna.fa Hydra_magnipapillata_rna.fa 
agalma catalog insert --id "NCBI_HYDMAG" --paths "Hydra_magnipapillata_rna.fa" --species "Hydra magnipapillata" --ncbi_id "6085" --itis_id "50845" --library_type "genome" --note "Gene predictions from genome sequencing"

