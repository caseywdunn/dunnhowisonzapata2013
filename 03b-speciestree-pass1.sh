#!/bin/bash
#SBATCH -t 24:00:00
#SBATCH -c 20
#SBATCH --mem=120G
#SBATCH -C intel
#SBATCH --exclusive

export AGALMA_DB="$PWD/agalma.sqlite"
export BIOLITE_RESOURCES="threads=${SLURM_CPUS_ON_NODE},memory=${SLURM_MEM_PER_NODE}M"

set -e

mkdir -p scratch
cd scratch

ID=AgalmaExampleTreeNoInform
TREE_ID=$(agalma diagnostics runid -n genetree | head -1)

agalma treeprune --id $ID --previous $TREE_ID
agalma multalign --id $ID
agalma supermatrix --id $ID
agalma supermatrix --id $ID --proportion 0.95
agalma speciestree --id $ID --outgroup Nematostella_vectensis --bootstrap 100

